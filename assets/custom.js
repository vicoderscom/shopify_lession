
$(function() {
    setTimeout(function() {
        $('.scrool-bottom').removeClass('hide').delay(2000);
        $('.delay-price').removeClass('hide').delay(2000);
    }, 2000);

    $('.scrool-bottom').click(function() {
        $("html, body").animate({ scrollTop: $(document).height() }, 1000);
    });

    $('li.collection-designers').click(function() {
        var title = $(this).attr('data-title');
        var text = $(this).text();
        var symbol = text.indexOf('+');
        if (symbol !== -1) {
            $(this).text(title + ' - ');
        } else {
            $(this).text(title + ' + ');
        }
        $(this).parent().find('.ul-sidebar').toggleClass('hide');
    });

    //thêm filte
    $('li.collection-filters').click(function() {
        var title = $(this).attr('data-title');
        var text = $(this).text();
        var symbol = text.indexOf('+');
        if (symbol !== -1) {
            $(this).text(title + ' - ');
        } else {
            $(this).text(title + ' + ');
        }
        $(this).parent().find('.ul-sidebar-filters').toggleClass('hide');
    });
    

    $('.checkbox-filters').click(function() {
        var class_designers = ($('.designers-li label.active-designers').attr('data-designers-label'));
        var checkbox_url = $(this).attr('data-url');
        var url = $(this).attr('data-sort');

        if (class_designers) {
            var tonghop = checkbox_url + class_designers + url;
            $(location).attr('href',tonghop);
        }else{
            var tonghop = checkbox_url + url;
            $(location).attr('href',tonghop);
        }
    });    

    $('.checkbox-filters-descending').click(function() {
        var class_designers2 = ($('.designers-li label.active-designers').attr('data-designers-label'));
        var checkbox_url_descending = $(this).attr('data-url-descending');
        var url_descending = $(this).attr('data-sort-descending');

        if (class_designers2) {
            var tonghop_descending = checkbox_url_descending + class_designers2 +url_descending;
            $(location).attr('href',tonghop_descending);
        }else{
            var tonghop_descending = checkbox_url_descending + url_descending;
            $(location).attr('href',tonghop_descending);
        }
    });


    $('.checkbox-filters-Latest').click(function() {
        var class_designers3 = ($('.designers-li label.active-designers').attr('data-designers-label'));
        var checkbox_url_created = $(this).attr('data-url-created');
        var url_created = $(this).attr('data-sort-created');

        if (class_designers3) {
            var tonghop_created = checkbox_url_created + class_designers3 + url_created;
            $(location).attr('href',tonghop_created);
        }else{
            var tonghop_created = checkbox_url_created + url_created;
            $(location).attr('href',tonghop_created);
        }
        
    });


    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    var tech = getUrlParameter('sort_by');
    $( ".li_filter" ).each(function() {
        var data_xs = $(this).find('.khiem').attr('data-sx');
        //console.log( data_xs + '__' + tech);
        if(data_xs == tech) {
            jQuery(".ul-sidebar-filters").removeClass('hide');
            jQuery(this).find('label').addClass('active-c');
        }
    });

    //console.log(tech);

    $('.designers-li').click(function(){

        var data_ds = $(this).find('input').attr('data-href');
        var data_sort_by1 = ($('.active-c').attr('data-sort-by'));

        if ( typeof data_sort_by1 === "undefined") {
            var connect = data_ds
            
            $(location).attr('href',connect);
        }else{
            var connect = data_ds + data_sort_by1;
            
            $(location).attr('href',connect);
        }
    });


    $('.designers-li').each(function() {
        var pathname = window.location.pathname;
        var data_url_vendor = $(this).find('.vonder-url').attr('data-href');

        if (pathname == data_url_vendor) {
            jQuery(".ul-sidebar-filters .ul-sidebar").removeClass('hide');

            jQuery(".custom-collection-template .ul-sidebar-filters").removeClass('hide');

            jQuery(this).find('label').addClass('active-designers');
        }
    });


    $('li.collection-designers-sidebar').click(function() {
        var title = $(this).attr('data-title-sidebar');
        var text = $(this).text();
        var symbol = text.indexOf('+');
        if (symbol !== -1) {
            $(this).text(' - ' + title);
        } else {
            $(this).text(' + ' + title);
        }
        $(this).parent().find('.ul-sidebar').toggleClass('hide');
    });


// js cho mobile
    $( ".filters-text-mb" ).each(function() {
        var data_xs_mb = $(this).attr('data-sx-mb');
        //console.log( data_xs_mb + '__' + tech);
        if(data_xs_mb == tech) {
            jQuery(this).addClass('active-mb');
            //jQuery('.mahfuz_designer_sidebar').show();
        }
    })


    $('.designers-md').click(function(){

        var data_href_mb = $(this).attr('data-href');
        var data_sort_by_md1 = ($('.active-mb').attr('data-sort-by_mb'));

        if (typeof data_sort_by_md1 === "undefined") {
            var connect2 = data_href_mb;

            $(location).attr('href',connect2);
        }else{
            var connect2 = data_href_mb + data_sort_by_md1;

            $(location).attr('href',connect2);
        }
        
    });

    $('.designers-md').each(function() {
        var pathname2 = window.location.pathname;
        var data_url_vendor2 = $(this).attr('data-href');

        if (pathname2 == data_url_vendor2) {
            jQuery(".mahfuz_designer_sidebar .ul-sidebar").removeClass('hide');
            jQuery(this).addClass('active-designers');
        }
    });

    $('li.collection-designers-mb').click(function() {
        $(this).parent().find('.ul-sidebar').toggleClass('hide');
    });


    var class_product = $('.grid__item').hasClass('custom-collection-empty');
    if ( class_product ) {
        
        $('#Collection .grid__item').css({"display":"flex" , "justify-content":"center" , "align-items":"center", "width":"80%"});
        
        $('.full-page ul').css({'position':'unset'});

        $('.custom-list-product').css({'display':'none'});
    }

    
    var title = $('li.collection-filters').attr('data-title');
    var text = $('li.collection-filters').text();
    var symbol = text.indexOf('+');
    var class_active_c  = $('.li_filter .price-designers').hasClass('active-c');

    var class_active_designers  = $('.ul-sidebar label').hasClass('active-designers');

    if (class_active_c || class_active_designers) {
        $('li.collection-filters').text(title + ' - ');
    } else {
        $('li.collection-filters').text(title + ' + ');
    }



    var title = $('.ul-sidebar-filters .collection-designers-sidebar').attr('data-title-sidebar');
    var text = $('.ul-sidebar-filters .collection-designers-sidebar').text();
    var symbol = text.indexOf('+');
    var class_active_designers  = $('.ul-sidebar label').hasClass('active-designers');
    var class_active_c  = $('.li_filter .price-designers').hasClass('active-c');

    if (class_active_designers) {
        $('.ul-sidebar-filters .collection-designers-sidebar').text(' - ' + title);
    } else {
        $('.ul-sidebar-filters .collection-designers-sidebar').text(' + ' + title);
    }


    var filterChk = $('.price-designers');

    filterChk.each(function(){
        if ($(this).hasClass('active-c') == true && $('.designchk').hasClass('active-designers') == false) {
            var test= $(this).attr('data-sort-by');

            var collectionUl = $('.collection li a');
            collectionUl.each(function(){
                var dataUrl = $(this).attr('href');
                dataUrl += test;
                $(this).attr('href', dataUrl);
            });  
        }
    });


    var designChk_dk = $('.designchk');

    designChk_dk.each(function() {
        if ($(this).hasClass('active-designers') == true && $('.price-designers').hasClass('active-c') == false) {
            var design_checkbox_dk = $(this).attr('data-designers-label');

            var ul_sidebarUl_dk = $('.collection li a');
            ul_sidebarUl_dk.each(function() {
                var dataUrl_dk = $(this).attr('href');

                dataUrl_dk += design_checkbox_dk;

                $(this).attr('href', dataUrl_dk);
            });
        }
    });


    var filter_design_all = $('.ul-sidebar-filters');

    filter_design_all.each(function() {
        if($('.designchk').hasClass('active-designers') == true && $('.price-designers').hasClass('active-c') == true){
            var design_chk_url = $('.designers-li').find('.active-designers').attr('data-designers-label');

            var filter_chk_url = $('.li_filter').find('.active-c').attr('data-sort-by');

            var ul_sidebarUl_all = $('.collection li a');
            ul_sidebarUl_all.each(function() {
                var data_url_href = $(this).attr('href');

                data_url_href += design_chk_url;
                data_url_href += filter_chk_url;

                $(this).attr('href', data_url_href);
            });
        }
    });


    $('.clear-all').click(function() {
        var data_clear_all = $(this).attr('data-clear');

        $(location).attr('href',data_clear_all);
    });

    // $('.url_all').click(function(event) {
    //     var url_all_desktop = $(this).attr('data-all');

    //     $(location).attr('href',url_all_desktop);
    // });


    var active_all = $('.test-1').hasClass('active');
    if (active_all) {
        $('.test-1').removeClass('active');

        $('.url_all').addClass('active');
    }


    var test = jQuery('.custom-list-product').text();
    var test_trim = test.trim();
     
    var text_sory = $('.grid__item').hasClass('custom-collection-empty');

     jQuery(document).ready(function(){
       if (test_trim === null || test_trim === '' && !text_sory) {
            jQuery('.custom-list-product').hide();
            jQuery('.full-page').append('<div class="grid__item small--text-center custom-collection-empty sory_product"><p class="page_boottoms" style="text-align: center;">Sorry, there are no products in this collection</p></div>');
            
            jQuery('.footer-desktop').css({'margin-top':'250px'});

            jQuery('.collection').addClass('menu_sory');

            //jQuery('.custom-list-product').css({'display':'none'});
        }                                    
     });


    
    // var list_product = $('.andy-grid').hasClass('custom-list-product')

    // if (list_product) {
    //     $('.footer-desktop').css({'margin-top':'500px'});
    // }else{
    //     console.log('no');
    // }

//js mobile

    var filterChk_mb = $('.price-designers');

    filterChk_mb.each(function(){
        if ($(this).hasClass('active-mb') == true && $('.designchk-mb').hasClass('active-designers') == false) {
            var test= $(this).attr('data-sort-by');

            var collectionUl_mb = $('.mahfuz_refine_sidebar .li-lv1 a');
            collectionUl_mb.each(function(){
                var dataUrl_mb = $(this).attr('href');
                dataUrl_mb += test;
                $(this).attr('href', dataUrl_mb);
            });  
        }
    });



    var designChk_mb = $('.designchk-mb');

    designChk_mb.each(function() {
        if ($(this).hasClass('active-designers') == true && $('.price-designers').hasClass('active-mb') == false) {
            var design_checkbox_mb = $(this).attr('data-designers-label-mb');

            var ul_sidebarUl_mb = $('.mahfuz_refine_sidebar .li-lv1 a');
            ul_sidebarUl_mb.each(function() {
                var dataUrl_2_mb = $(this).attr('href');

                dataUrl_2_mb += design_checkbox_mb;

                $(this).attr('href', dataUrl_2_mb);
            });
        }
    });

    var filter_design_all_mb = $('.mahfuz_designer_sidebar');

    filter_design_all_mb.each(function() {
        if($('.designchk-mb').hasClass('active-designers') == true && $('.price-designers').hasClass('active-c') == true){
            var design_chk_url_mb = ($('.active-mb').attr('data-sort-by'));

            var filter_chk_url_mb = ($('.active-designers').attr('data-designers-label-mb'));

            var ul_sidebarUl_all_mb = $('.mahfuz_refine_sidebar .li-lv1 a');
            ul_sidebarUl_all_mb.each(function() {
                var data_url_href_mb = $(this).attr('href');

                data_url_href_mb += filter_chk_url_mb;
                data_url_href_mb += design_chk_url_mb;

                $(this).attr('href', data_url_href_mb);
            });
        }
    });



    // var filterList = $('.ul-sidebar.design>li');

    //   filterList.off();
    //   filterList = $('.ul-sidebar.design>li label');
    //   filterList.click(function(event) {
    //     var currentUrl = window.location.href;
    //     var vendor = $(event.target).attr('data-designers-label');

    //     currentUrl = currentUrl.substring(0, currentUrl.indexOf('?q') + 3);
    //     vendor = vendor.substring(1, vendor.length);
    //     currentUrl = currentUrl + vendor;
    //     console.log(currentUrl);
    //     window.location.href = currentUrl;

    //   });


// var stickyNavTop = $('#shopify-section-header').offset().top;
 
// var stickyNav = function(){
// var scrollTop = $(window).scrollTop();
      
// if (scrollTop > stickyNavTop) { 
//     $('#shopify-section-header').addClass('sticky');
// } else {
//     $('#shopify-section-header').removeClass('sticky'); 
// }
// };
 
// stickyNav();
 
// $(window).scroll(function() {
//   stickyNav();
// });
  
  // mobile menu
// jQuery(document).ready(function(){
// 	jQuery('.menu-button').click(function(){
//       jQuery('.mahfuz_mobile_menu').css('top', '60px');
// 	});
// //   jQuery(".custom-collection-template").stick_in_parent();
// });
  
jQuery(document).ready(function(){
	jQuery('.croxx_menu').click(function(){
      jQuery('.mahfuz_mobile_menu').css('top', '-100%');
	})
});

  jQuery(document).ready(function(){
	jQuery('.mahfuz_refine_btn').click(function(){
		jQuery('.mahfuz_refine_sidebar').slideToggle('slow');
	})
});
    jQuery(document).ready(function(){
	jQuery('.mahfuz_designer_btn').click(function(){
		jQuery('.mahfuz_designer_sidebar').slideToggle('slow');
	})
});
  
  
  
  
});
